/*
  8_IngresoIndefinido
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Octavo programa. Solicita al usuario el ingreso de una nota, y cuenta la cantidad de notas ingresadas.
    Ademas informa si las mismas corresponden a examenes aprobados o desaprobados.
    Agrega calculo de promedio, nota maxima y nota minima
    Utiliza un bucle while() hasta que se ingresa una nota invalida
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

int nota;
int cantidad = 0;	// Inicializo el contador en cero
int notaMax = 0;	// Inicializo esta variable en la menor nota posible
int notaMin = 10;	// Inicializo esta variable en la mayor nota posible
int suma = 0;
float promedio;

int main(void)
{
	while(nota <= 10)
	{
		printf("Ingrese nota: ");
		scanf("%d", &nota);

		if(nota > 0 && nota <= 10)
		{
			cantidad = cantidad + 1;
			//printf("Cantidad hasta ahora: %d\n", cantidad);

			suma = suma + nota;

			// Actualizo mayor o menor nota
			if(nota > notaMax)
			{
				notaMax = nota;
			}

			if(nota < notaMin)
			{
				notaMin = nota;
			}
		}

		if(nota >= 6 && nota <= 10)
		{
			printf("Aprobado!\n");
		}

		if (nota < 6 && nota > 0)
		{
			printf("Desaprobado :(\n");
		}
	}

	promedio = (float) suma / cantidad;

	printf("Cantidad de notas ingresadas: %d\n", cantidad);
	printf("Promedio de notas: %f\n", promedio);
	printf("Mayor nota: %d\n", notaMax);
	printf("Menor nota: %d\n", notaMin);

	printf("Fin del programa\n");
	return 0;
}