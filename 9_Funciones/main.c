/*
  9_Funciones
  
  Autor: Ailin Ugolini
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Noveno programa. Solicita al usuario el ingreso de una nota, y cuenta la cantidad de notas ingresadas.
    Ademas informa si las mismas corresponden a examenes aprobados o desaprobados.
    Agrega calculo de promedio, nota maxima y nota minima
    Utiliza un bucle while() hasta que se ingresa una nota invalida
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>
#include <string.h>

// Prototipos de funciones
int Jubilacion(int edad);
void Bienvenida(int usr);

// Variables de programa
int i;
int edad;
int ret;

int main(void)
{

	for(i = 1; i < 5; i++)
	{
		Bienvenida(i);
		printf("\nIngrese su edad: ");
		scanf("%d", &edad);
		ret = Jubilacion(edad);
		if(ret == 1)
		{
			printf("Se puede jubilar\n");
		}
		else
		{
			printf("Todavia falta para jubilarse\n");
		}
	}
}


/* ----------------------------------------------------------------------------------------------------- */

/**
 * @brief      Verifica si el usuario esta en posibilidad de jubilacion
 * @param[in]  edad  Edad del usuario
 * @return     1 si se puede jubilar, 0 si no
 */
int Jubilacion(int edad)
{
	if(edad >= 65)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


/**
 * @brief      Imprime un mensaje de bienvenida
 * @param[in]  i     Identificador de usuario
 */
void Bienvenida(int usr)
{
	printf("Hola usuario %d ", usr);
}
