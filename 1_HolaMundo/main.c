/*
  1_HolaMundo
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Primer programa. Imprime un texto en pantalla
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>	// Proporciona la funcion printf()


// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	// La funcion printf() recibe texto entre comillas dobles, que mostrara en pantalla
	printf("Hola mundo!\n");

	printf("Este print no salta de renglon. ");
	printf("Este si! Tiene el \\n \n");

	printf("Chau mundo cruel\n");

	// Fin de la funcion main. Retorna 0
	return 0;
}