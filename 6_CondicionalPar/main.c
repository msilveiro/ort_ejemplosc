/*
  6_CondicionalPar
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Sexto programa. Solicita al usuario el ingreso de un numero, y verifica si es par o impar
    Utiliza un condicional if para el testeo
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
int miNumero;
int resto;

// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	printf("Ingrese un numero cualquiera: ");
	scanf("%d", &miNumero);

	// Calculo el resto del numero con el operador modulo
	resto = miNumero % 2;

	// Si el resto es 0, significa que el numero es par
	if(resto == 0)
	{
		printf("El numero es par\n");
	}

	// Si el resto es 1, significa que el numero es impar
	if(resto == 1)
	{
		printf("El numero es impar\n");
	}

	// Fin de la funcion main. Retorna 0
	return 0;
}