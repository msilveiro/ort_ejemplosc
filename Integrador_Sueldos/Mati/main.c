/*
  Programa integrador - Liquidacion de sueldos
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    TODO
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Variables del programa
float mayor1 = 0;
float mayor2 = 0;
float mayor3 = 0;

float menor1 = 0;
float menor2 = 0;
float menor3 = 0;

float prom1 = 0;
float prom2 = 0;
float prom3 = 0;

int cant1 = 0;
int cant2 = 0;
int cant3 = 0;

// Prototipos de funciones
void printEncabezado(void);
void printTabla(void);
float calcularSueldo(int tipo, float ventas);

/* ----------------------------------------------------------------------------------------------------- */

/**
 * @brief      Funcion main: punto de entrada de nuestro programa
 *
 * @param[in]  argc  No utilizado
 * @param      argv  No utilizado
 *
 * @return     0
 */
int main(int argc, char const *argv[])
{
	int trabajador = 1;
	int tipo = 0;
	float sueldo;
	float ventas;

	printEncabezado();

	while(trabajador != 0)
	{
		printf("\nIngrese el tipo de trabajador: ");
		scanf("%d", &trabajador);

		// Sigo el lazo si el tipo de trabajador es valido
		if(trabajador > 0 && trabajador < 4)
		{
			printf("Ingrese el monto de ventas: ");
			scanf("%f", &ventas);

			sueldo = calcularSueldo(trabajador, ventas);
			printf("El sueldo de este trabajador es %.2f\n", sueldo);

			if(trabajador == 1)
			{
				if(sueldo > mayor1)
				{
					mayor1 = sueldo;
				}
				if(sueldo < menor1 || cant1 == 0)
				{
					menor1 = sueldo;
				}
				prom1 = prom1 + sueldo;
				cant1 = cant1 + 1;
			}

			if(trabajador == 2)
			{
				if(sueldo > mayor2)
				{
					mayor2 = sueldo;
				}
				if(sueldo < menor2 || cant2 == 0)
				{
					menor2 = sueldo;
				}
				prom2 = prom2 + sueldo;
				cant2 = cant2 + 1;
			}

			if(trabajador == 3)
			{
				if(sueldo > mayor3)
				{
					mayor3 = sueldo;
				}
				if(sueldo < menor3 || cant3 == 0)
				{
					menor3 = sueldo;
				}
				prom3 = prom3 + sueldo;
				cant3 = cant3 + 1;
			}
		}
		else if (trabajador != 0)
		{
			printf("Tipo de trabajador erroneo.\n");
		}
	}

	// Para evitar division por cero
	if(cant1 > 0)
		prom1 = prom1 / cant1;
	
	if(cant2 > 0)
		prom2 = prom2 / cant2;
	
	if(cant3 > 0)
		prom3 = prom3 / cant3;

	printTabla();

	return 0;
}

/* ----------------------------------------------------------------------------------------------------- */

/**
 * @brief      Calcula el sueldo por mes de un empleado
 *
 * @param[in]  tipo    Tipo de empleado: 1, 2 o 3
 * @param[in]  ventas  Monto total obtenido por las ventas
 *
 * @return     Sueldo mensual del empleado
 */
float calcularSueldo(int tipo, float ventas)
{
	float sueldo = 0;
	float comision;

	if(tipo == 1)
	{
		comision = ventas * 0.05;
		sueldo = 1000.0 + comision;
	}
	if(tipo == 2)
	{
		comision = ventas * 0.1;
		sueldo = 500.0 + comision;
	}
	if(tipo == 3)
	{
		comision = ventas * 0.2;
		sueldo = 200.0 + comision;
	}

	return sueldo;
}


/**
 * @brief      Imprime un mensaje de bienvenida del programa
 */
void printEncabezado(void)
{
	printf("\n#------------------------------------------#\n");
	printf(  "#          Liquidacion de sueldos          #");
	printf("\n#------------------------------------------#\n");
	printf("Tipos de trabajadores:\n");
	printf("Tipo 1: $1000 + 5%% ventas\n");
	printf("Tipo 2: $500 + 10%% ventas\n");
	printf("Tipo 3: $200 + 20%% ventas\n");
	printf("\n#------------------------------------------#\n");
}



/**
 * @brief      Imprime la tabla de resultados del programa
 */
void printTabla(void)
{
	printf("\n#------------------------------------------#\n");
	printf("\nTabla de resultados:\n\n");
	printf("#############################################\n");
	printf("# Tipo  #   Mayor   #   Menor   #  Promedio #\n");
	printf("#-------#-----------#-----------#-----------#\n");
	printf("#   1   # %9.2f # %9.2f # %9.2f #\n", mayor1, menor1, prom1);
	printf("#   2   # %9.2f # %9.2f # %9.2f #\n", mayor2, menor2, prom2);
	printf("#   3   # %9.2f # %9.2f # %9.2f #\n", mayor3, menor3, prom3);
	printf("#-------#-----------#-----------#-----------#\n");
	printf("# Total # %9.2f # %9.2f # %9.2f #\n", mayor1, menor1, prom1);
	printf("#############################################\n");
}