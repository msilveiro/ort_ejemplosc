/*
  Programa integrador - Liquidacion de sueldos
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    TODO
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Prototipos de funciones
void Descripcion(void);
float Sueldo(int vendedor, float dineroGenerado);

/* ----------------------------------------------------------------------------------------------------- */

int main(void) 
{
	int trabajador= 1;
	int cantidad1= 0;
	int cantidad2= 0;
	int cantidad3= 0;
	float dinero= 0;
	float comision= 0;
	float mayor1= 0;
	float mayor2= 0;
	float mayor3= 0;
	float menor1= 0;
	float menor2= 0;
	float menor3= 0;
	float promedio1= 0;
	float promedio2= 0;
	float promedio3= 0;

	Descripcion();
	
	while( trabajador != 0)
	{
		printf("\n Ingrese el tipo de trabajador:");
		scanf("%d", &trabajador);
		
		if( trabajador!= 1 && trabajador!= 2 && trabajador!= 3 )
		{
			if(cantidad1!=0 ) 
			{
				promedio1= promedio1/cantidad1;  
			}
			if(cantidad2!=0 ) 
			{
				promedio2= promedio2/cantidad2;  
			}
			if(cantidad3!=0 ) 
			{
				promedio3= promedio3/cantidad3;  
			}

			printf ("       | \t 1 \t\t | \t 2 \t\t | \t 3 \t\t |  \n ");
			printf ("Mayor | \t %.2f \t | \t %.2f \t |\t %.2f \t | \n", mayor1,mayor2,mayor3);
			printf (" Menor | \t %.2f \t | \t %.2f \t |\t %.2f \t | \n", menor1,menor2,menor3);
			printf (" Prome | \t %.2f \t | \t %.2f \t |\t %.2f \t | \n", promedio1,promedio2,promedio3);
			
			return 0;
		}

		printf("Ingrese el dinero que le generó al local: ");
		scanf("%f", &dinero);
		
		comision= Sueldo(trabajador,dinero);
		printf("El sueldo del trabajador del tipo %d es %f",trabajador, comision);

		if( trabajador == 1)
		{
			cantidad1= cantidad1 +1;
			if( comision>= mayor1)
			{
				mayor1= comision;
			}
			if( comision <= menor1 || cantidad1==1 )
			{
				menor1= comision;
			}
			promedio1= promedio1 + comision;
		}
		if( trabajador == 2)
		{
			cantidad2= cantidad2 +1;
			if( comision>= mayor2 )
			{
				mayor2= comision;
			}
			if( comision<= menor2 || cantidad2==1)
			{
				menor2= comision;
			}
			promedio2= promedio2 + comision;
		}
		if( trabajador == 3)
		{
			cantidad3= cantidad3 +1;
			if( comision>= mayor3)
			{
				mayor3= comision;
			}
			if( comision<= menor3 || cantidad3==1)
			{
				menor3= comision;
			}
			promedio3= promedio3 + comision;
		}

	}
	
	return 0;
}

void Descripcion(void)
{
	printf("\n Los tipos de trabajadores son \n \t Tipo 1: sueldo basico de $1000 \n \t Tipo 2: sueldo basico de $500 \n \t Tipo 3: sueldo basico de $200 \n \n");
	return;
}

float Sueldo(int vendedor, float dineroGenerado)
{
  float sueldo;
  if(vendedor==1)
  {
    sueldo= 1000+ dineroGenerado*0.05;
    return sueldo;
  }
  if(vendedor==2)
  {
    sueldo= 500+ dineroGenerado*0.1;
    return sueldo;
  }
  if(vendedor==3)
  {
    sueldo= 200+ dineroGenerado*0.2;
    return sueldo;
  }
  else
  {
    printf("Terminó el programa");
    return 0;
  }
}