/*
  3_OpsNumericas
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Tercer programa. Muestra operaciones basicas con dos numeros enteros
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
int num1 = 8;
int num2 = 5;
int resultado;

// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	// Suma
	resultado = num1 + num2;
	printf("La suma de %d y %d es: %d\n", num1, num2, resultado);

	// Resta
	resultado = num1 - num2;
	printf("La resta de %d y %d es: %d\n", num1, num2, resultado);

	// Multiplicacion
	resultado = num1 * num2;
	printf("El producto de %d y %d es: %d\n", num1, num2, resultado);

	// Division
	resultado = num1 / num2;
	printf("La division de %d y %d es: %d\n", num1, num2, resultado);

	// Fin de la funcion main. Retorna 0
	return 0;
}