/*
  2_PrintVariable
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Segundo programa. Declaro distintos tipos de variables, y las imprimo en pantalla
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
int miEntero;
char miLetra;
float miFloat;


// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	// Asigno valores a las variables
	miEntero = 4;
	miLetra = 'R';		// Las letras se escriben entre comillas simples. 1 SOLA LETRA!!!!
	miFloat = 2.4;	// Los numeros con 'coma' se escriben con 'punto' (notacion americana)

	// La funcion printf() recibe texto entre comillas dobles, que mostrara en pantalla
	printf("Mi numero entero es: %d \n", miEntero);
	printf("Mi letra es: %c \n", miLetra);
	printf("Mi numero real es: %f \n", miFloat);

	printf("\nPuedo imprimir varias variables en un mismo print, miren!\n");
	printf("Entero: %d , Letra: %c , Float: %f \n", miEntero, miLetra, miFloat);

	// Fin de la funcion main. Retorna 0
	return 0;
}