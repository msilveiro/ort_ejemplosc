/*
  Integrador_PromedioNotas
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Programa integrador de los ejemplos basicos.
    Solicita el ingreso de 4 materias, validando que las notas ingresadas sean validas.
    Informa el promedio de las materias, cuantas estan aprobadas y cuantas estan desaprobadas.
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
float notaMatematica;
float notaLengua;
float notaLDD;
float notaTI;

float promedio;
int cantAprob = 0;
int cantDesaprob = 0;

// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	// Ingreso de notas con checkeo de errores
	// Se queda loopeando hasta que ingreso un valor correcto
	do {
		printf("Ingrese la nota de matematica: ");
		scanf("%f", &notaMatematica);
	} while(notaMatematica < 0 || notaMatematica > 10);

	do {
		printf("Ingrese la nota de lengua: ");
		scanf("%f", &notaLengua);
	} while(notaLengua < 0 && notaLengua > 10);

	do {
		printf("Ingrese la nota de LDD: ");
		scanf("%f", &notaLDD);
	} while(notaLDD < 0 && notaLDD > 10);

	do {
		printf("Ingrese la nota de TI: ");
		scanf("%f", &notaTI);
	} while(notaTI < 0 && notaTI > 10);

	// Calculo del promedio
	promedio = (notaMatematica + notaLengua + notaLDD + notaTI) / 4;

	// Cantidad de materias aprobadas
	if(notaMatematica >= 6)
	{
		cantAprob = cantAprob + 1;
	}

	if(notaLengua >= 6)
	{
		cantAprob = cantAprob + 1;
	}

	if(notaLDD >= 6)
	{
		cantAprob = cantAprob + 1;
	}

	if(notaTI >= 6)
	{
		cantAprob = cantAprob + 1;
	}

	// Cantidad de materias desaprobadas
	cantDesaprob = 4 - cantAprob;

	printf("\nEl promedio de las notas es: %.2f\n", promedio);
	printf("Tenes %d aprobadas, y %d desaprobadas.\n", cantAprob, cantDesaprob);

	// Fin de la funcion main. Retorna 0
	return 0;
}