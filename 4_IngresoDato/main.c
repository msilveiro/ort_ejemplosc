/*
  4_IngresoDato
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Cuarto programa. Programa que solicita al usuario el ingreso de varios tipos de datos
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
char miMateria[30];
float miNota;
int miAnio;

// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	printf("Ingrese el nombre de la materia: ");
	scanf("%s", miMateria);

	printf("Ingrese la nota de la materia: ");
	scanf("%f", &miNota);

	printf("Ingrese el anio de la materia: ");
	scanf("%d", &miAnio);

	printf("\nTe sacaste un %f en la materia %s del anio %d\n", miNota, miMateria, miAnio);

	// Fin de la funcion main. Retorna 0
	return 0;
}