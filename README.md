# ORT_EjemplosC

Codigos fuente de ejemplos en C que cubren desde conceptos basicos hasta programas mas avanzados.

## Ejemplos disponibles:
1. Impresion de texto en pantalla.
2. Impresion de varios tipos de variables en pantalla.
3. Operaciones basicas entre variables numericas.
4. Ingreso desde consola de varios tipos de datos.
5. Ingreso de 4 notas de materias, y calculo del promedio.
6. Ejemplo de uso de if, verificando paridad de un numero entero.
7. Ejemplo de uso de while, solicitando notas hasta una condicion de fin de programa.
8. Ampliacion del ejemplo anterior, calculando diferentes parametros de las notas.
9. Ejemplo de declaracion de funciones en C.
Ademas, se ofrecen dos ejemplos integradores de los temas vistos en el curso:
1. Promedio de notas, con distinto tipo de validacion de datos.
2. Programa para liquidacion de sueldos, con salida tipo tabla en ASCII.

## Acknowledgements
Este repositorio fue posible gracias a los contenidos desarrollados en la materia de Tecnologias de la Informacion de la Escuela ORT, Orientacion en Mecatronica.

## Autor
Matias Silveiro