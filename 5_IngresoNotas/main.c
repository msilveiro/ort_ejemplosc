/*
  5_IngresoNotas
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Quinto programa. Programa que solicita al usuario el ingreso de varias notas, e informa el promedio
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

// Declaracion de variables
float notaMatematica;
float notaLengua;
float notaLDD;
float notaTI;
float promedio;

// Funcion main: punto de entrada de nuestro programa
int main(void)
{
	printf("Ingrese la nota de matematica: ");
	scanf("%f", &notaMatematica);

	printf("Ingrese la nota de lengua: ");
	scanf("%f", &notaLengua);

	printf("Ingrese la nota de LDD: ");
	scanf("%f", &notaLDD);

	printf("Ingrese la nota de TI: ");
	scanf("%f", &notaTI);

	promedio = (notaMatematica + notaLengua + notaLDD + notaTI) / 4;

	printf("\nEl promedio de las notas es: %.2f\n", promedio);

	// Fin de la funcion main. Retorna 0
	return 0;
}