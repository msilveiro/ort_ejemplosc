/*
  7_IngresoIndefinido
  
  Autor: Matias Silveiro
  Tecnologia de la Informacion - Escuela ORT
  
  Descripcion:
    Septimo programa. Solicita al usuario el ingreso de una nota, y cuenta la cantidad de notas ingresadas.
    Ademas informa si las mismas corresponden a examenes aprobados o desaprobados.
    Utiliza un bucle while() hasta que se ingresa una nota invalida
*/

/* ----------------------------------------------------------------------------------------------------- */

// Inclusion de librerias necesarias
#include <stdio.h>

int nota = 0;
int cantidad = 0;

int main(void)
{
	while(nota <= 10)
	{
		printf("Ingrese nota: ");
		scanf("%d", &nota);

		if(nota > 0 && nota <= 10)
		{
			cantidad = cantidad + 1;
			//printf("Cantidad hasta ahora: %d\n", cantidad);
		}

		if(nota >= 6 && nota <= 10)
		{
			printf("Aprobado!\n");
		}

		if (nota < 6 && nota > 0)
		{
			printf("Desaprobado :(\n");
		}
	}

	printf("Cantidad de notas ingresadas: %d\n", cantidad);
	printf("Fin del programa\n");

	return 0;
}